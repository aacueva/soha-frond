<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- M A T E R I A L I Z E -->
        <link href="{{ asset('css/materialize.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
  		<link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
  		<link href="{{asset('css/style-add.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
  		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>SOHA</title>
    </head>
    <body >



	<nav class="white" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo"><img  src="{{ asset('img/logo.png') }}" alt=""></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Inicio</a></li>
        <li><a href="#">Soluciones</a></li>
        <li><a href="#">Portafolio</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Nosotros</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Inicio</a></li>
        <li><a href="#">Soluciones</a></li>
        <li><a href="#">Portafolio</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Nosotros</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>


<!-- INICIA LA SECCION DE LOS SERVICIOS -->
  <div class="container-fluid">
    <div class="section">
		<div class="row ">
			<div class="col m6 s12 service programacion">
				<div class="concept">
					<h1>Desarrollo Web</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus vel suscipit quam qui, minima enim error deserunt ea totam. Assumenda quos ut quo nulla sunt et obcaecati animi, deleniti non!</p>
					<a href="">Saber mas</a>					
				</div>
					<img src="{{ asset('img/desarrollo.png')}}" alt="">	
			</div>
			<div class="col m6 s12 service diseño">
				<div class="concept">
					<h1>Artes Graficas</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus vel suscipit quam qui, minima enim error deserunt ea totam. Assumenda quos ut quo nulla sunt et obcaecati animi, deleniti non!</p>
					<a href="">Saber mas</a>					
				</div>
				<img src="{{ asset('img/diseño.png')}}" alt="">
			</div>
		</div>
		<div class="row margin-row">
			<div class="col m6 s12 service iot">
				<div class="concept">
					<h1>Internet Of  Things</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus vel suscipit quam qui, minima enim error deserunt ea totam. Assumenda quos ut quo nulla sunt et obcaecati animi, deleniti non!</p>
					<a href="">Saber mas</a>					
				</div>
				<img src="{{ asset('img/iot.png')}}" alt="">
			</div>
			<div class="col m6 s12 service social">
				<div class="concept">
					<h1>Marketing Digital</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus vel suscipit quam qui, minima enim error deserunt ea totam. Assumenda quos ut quo nulla sunt et obcaecati animi, deleniti non!</p>	
					<a href="">Saber mas</a>				
				</div>
				<img src="{{ asset('img/marketing.png')}}" alt="">
			</div>
		</div>
    </div>
  </div>
<!-- FINALIZA LA SECCION DE LOS SERVICIOS -->

<!-- INICIA LA SECCION DE PORTAFOLIO -->
	<div class="container">
		<div class="title">
				<!-- TITULO DE LA SECCION -->
				<h1 class="proyectos">Proyectos</h1>
		</div>
		<div class="section">
			<div class="row">
				<div class="col l4 m6 s12	">
						<div class="card small z-depth-4">
						    <div class="card-image waves-effect waves-block waves-light">
						      <img class="activator" src="{{ asset('img/proyectos/proyecto1.png')}}">
						    </div>
						    <div class="card-content">
						      <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
						      <p><a href="#">This is a link</a></p>
						    </div>
						    <div class="card-reveal">
						      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
						      <p>Here is some more information about this product that is only revealed once clicked on.</p>
						    </div>
		  				</div>
				</div>
				<div class="col l4 m6 s12	">
						<div class="card small z-depth-4">
						    <div class="card-image waves-effect waves-block waves-light">
						      <img class="activator" src="{{ asset('img/proyectos/proyecto2.png')}}">
						    </div>
						    <div class="card-content">
						      <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
						      <p><a href="#">This is a link</a></p>
						    </div>
						    <div class="card-reveal">
						      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
						      <p>Here is some more information about this product that is only revealed once clicked on.</p>
						    </div>
		  				</div>
				</div>
				<div class="col l4 m6 s12	">
						<div class="card small z-depth-4">
						    <div class="card-image waves-effect waves-block waves-light">
						      <img class="activator" src="{{ asset('img/proyectos/proyecto3.png')}}">
						    </div>
						    <div class="card-content">
						      <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
						    </div>
						    <div class="card-reveal">
						      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
						      <p>Here is some more information about this product that is only revealed once clicked on.</p>
						    </div>
		  				</div>
				</div>
				<div class="col l4 m6 s12	">
						<div class="card small z-depth-4">
						    <div class="card-image waves-effect waves-block waves-light">
						      <img class="activator" src="{{ asset('img/proyectos/proyecto1.png')}}">
						    </div>
						    <div class="card-content">
						      <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
						      <p><a href="#">This is a link</a></p>
						    </div>
						    <div class="card-reveal">
						      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
						      <p>Here is some more information about this product that is only revealed once clicked on.</p>
						    </div>
		  				</div>
				</div>
				<div class="col l4 m6 s12	">
						<div class="card small z-depth-4">
						    <div class="card-image waves-effect waves-block waves-light">
						      <img class="activator" src="{{ asset('img/proyectos/proyecto2.png')}}">
						    </div>
						    <div class="card-content">
						      <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
						      <p><a href="#">This is a link</a></p>
						    </div>
						    <div class="card-reveal">
						      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
						      <p>Here is some more information about this product that is only revealed once clicked on.</p>
						    </div>
		  				</div>
				</div>
				<div class="col l4 m6 s12	">
						<div class="card small z-depth-4">
						    <div class="card-image waves-effect waves-block waves-light">
						      <img class="activator" src="{{ asset('img/proyectos/proyecto3.png')}}">
						    </div>
						    <div class="card-content">
						      <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
						    </div>
						    <div class="card-reveal">
						      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
						      <p>Here is some more information about this product that is only revealed once clicked on.</p>
						    </div>
		  				</div>
				</div>
			</div>
			<div class="row margin-row">
				<div class="col m12 btn-proyecto">
					<a href="">Mas Proyectos</a>
				</div>
			</div>
		</div>
	</div>


<!-- FINALIZA LA SECCION DE PORTAFOLIO -->
<!-- S E P A R A D O R -->
<img class="clip-me" src="{{ asset('img/separator.jpg')}}">

<!-- INICIANDO SECCION DE NOSOTROS -->
	
	<div class="container">
		<div class="title">
			<h1>NOSOTROS</h1>
		</div>
		<div class="nosotros">
			<div class="nosotros-parrafo">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium ducimus officiis iusto maxime quo pariatur iste sit et aliquam sint, minus corporis beatae velit quia saepe perferendis repellat molestiae ipsum.</p>			
			</div>
		</div>

		<div class="row ">
				<div class="col l6  m6 s12 center-align  margin-row">
						<img class="img-user" src="{{ asset('img/equipo/percy.png')}}" alt="">
						<h4>Percy Tejada 🇵🇪</h4>
						<h5>CEO / Founder</h5>
						<h5>Backend Developer</h5>
				</div>
				<div class="col l6 m6 s12 center-align margin-row">
						<img class="img-user" src="{{ asset('img/equipo/samuel.png')}}" alt="">
						<h4>Samuel Garcia 🇻🇪</h4>
						<h5>CMO</h5>
						<h5>FullStack Developer</h5>
				</div>
		</div>
		<div class="row ">
				<div class="col l4 m6 s12 center-align margin-row">
					<img class="img-user" src="{{ asset('img/equipo/aaron.png')}}" alt="">
					<h4>Aaron Cueva 🇵🇪</h4>
					<h5>CFO / CO-Founder</h5>
					<h5>FullStack Developer</h5>
				</div>
				<div class="col l4 m6 s12 center-align margin-row">
					<img class="img-user" src="{{ asset('img/equipo/kendal.png')}}" alt="">
					<h4>Kendal Vargas 🇦🇷</h4>
					<h5>CCO / Founder</h5>
					<h5>Backend Developer</h5>
				</div>
				<div class="col  l4 m12 s12 center-align margin-row">
					<img class="img-user" src="{{ asset('img/equipo/david.png')}}" alt="">
					<h4>David Ramirez 🇵🇪</h4>
					<h5>CEO / Founder</h5>
					<h5>Backend Developer</h5>
				</div>
		</div>
	</div>

<div class="container-fluid">
	<div class="row">
		<div class="col l6 s12 map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1940.5690402104344!2d-76.1294522528092!3d-13.403775690795293!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2spe!4v1566319254880!5m2!1ses-419!2spe" width="100%" height="700" frameborder="0" style="border:0" allowfullscreen>
			</iframe>
		</div>
		<div class="col l6 s12 form">
			<div class="box-form">
					<h3>Contactanos</h3>
						<form action="">
							<div class="input-field col s6">
					          <input id="icon_prefix" type="text" class="validate">
					          <label for="icon_prefix">Nombre</label>
					        </div>
					        <div class="input-field col s6">
					          <input id="icon_telephone" type="tel" class="validate">
					          <label for="icon_prefix">Apellido</label>
					        </div>
					        <div class="input-field col s6">
					        	<input id="email" type="email" class="validate">
	          					<label for="email">Email</label>
					        </div>
					        <div class="input-field col s6">
						        <input id="icon_telephone" type="tel" class="validate">
						        <label for="icon_telephone">Telephone</label>
					        </div>
					        <div class="input-field col s12">
					         	<textarea id="textarea1" class="materialize-textarea"></textarea>
					          	<label for="textarea1">Textarea</label>
					        </div>
					        <a class="btn-floating btn-large waves-effect waves-light blue 	"><i class="material-icons right">send</i></a>
						</form>
			</div>		
		</div>
	</div>
</div>




<!-- FINALIZANDO SECCION NOSOTROS-->
<!--
  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="{{asset('img/background2.jpg')}}" alt="Unsplashed background img 2"></div>
  </div>

<div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>
          <h4>Contact Us</h4>
          <p class="left-align light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam scelerisque id nunc nec volutpat. Etiam pellentesque tristique arcu, non consequat magna fermentum ac. Cras ut ultricies eros. Maecenas eros justo, ullamcorper a sapien id, viverra ultrices eros. Morbi sem neque, posuere et pretium eget, bibendum sollicitudin lacus. Aliquam eleifend sollicitudin diam, eu mattis nisl maximus sed. Nulla imperdiet semper molestie. Morbi massa odio, condimentum sed ipsum ac, gravida ultrices erat. Nullam eget dignissim mauris, non tristique erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
        </div>
      </div>
  	</div>
</div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="background3.jpg" alt="Unsplashed background img 3"></div>
  </div>

  <footer class="page-footer teal">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Company Bio</h5>
          <p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Settings</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Connect</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="brown-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>

      <!-- Element Showed -->

  <!-- Tap Target Structure -->
  <!--- <div class="tap-target" data-target="menu">
    <div class="tap-target-content">
      <h5>Title</h5>
      <p>A bunch of text</p>
    </div>
  </div>

-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  	<script src="{{ asset('js/materialize.js')}}"></script>
  	<script src="{{ asset('js/init.js')}}"></script>
  	<script>
  		$(document).ready(function(){
    		$('.tap-target').tapTarget();
  		});
  		
  	</script>
	</body>
</html>
